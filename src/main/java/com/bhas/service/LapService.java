package com.bhas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhas.modal.Lap;
import com.bhas.repo.LapRepo;

@Service
public class LapService 
{
	// DI,
	@Autowired
	private LapRepo lapRepo;
	
	// CRUD,
	// 1. INSERT INTO TABLE-NAME VALUES();
		public Lap insertLaps(Lap lap)
		{
			return lapRepo.save(lap);
		}
}
