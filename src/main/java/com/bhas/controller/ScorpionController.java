package com.bhas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bhas.dto.ResponseDTO;
import com.bhas.modal.Lap;
import com.bhas.service.LapService;

@RestController
public class ScorpionController 
{
	// DI,
	@Autowired
	private LapService lapService;
	
	// test endPoint,
	@GetMapping
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public String greetMsg()
	{
		System.out.println("\n*********\n");
		return "Welcome to the Emperor-Scorpion Application,,,,";
	}
	
	// postRequest,
	@PostMapping("/insertLap")
	public ResponseEntity<ResponseDTO> insertLap(@RequestBody Lap lap)
	{
		return new ResponseEntity<>(new ResponseDTO("Lap saved,", lapService.insertLaps(lap)),HttpStatus.CREATED);
	}

}
