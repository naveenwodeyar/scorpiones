package com.bhas.modal;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Company 
{
	@Id
	private int compId;
	private String compName;
	private String compMail;
	private String compAddress;
	
}
