package com.bhas.modal;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Lap 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lapId;
	private String lapName;
	private String lapMake;
	private double lapPrice;
	private int lapModel;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Company company;
}
