package com.bhas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestDTO 
{
	private int compId;
	private String compName;
	private String compMail;
	private String compAddress;
}
