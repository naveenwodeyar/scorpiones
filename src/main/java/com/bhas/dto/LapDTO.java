package com.bhas.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LapDTO
{
	private int lapId;
	private String lapName;
	private String lapMake;
	private double lapPrice;
	private int lapModel;
}
