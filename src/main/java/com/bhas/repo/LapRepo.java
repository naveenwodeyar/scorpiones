package com.bhas.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhas.modal.Lap;

@Repository
public interface LapRepo extends JpaRepository<Lap, Integer>
{

}
